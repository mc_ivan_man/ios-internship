//
//  MyURLSession.swift
//  ios-internship
//
//  Created by Ivan on 24.06.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import Foundation

class MyURLSession{
    typealias completion = (Data?, HTTPURLResponse?, Error?)-> Void
    
    static var urlSession = MyURLSession()
    
    func getData(request: URLRequest, completionHandler: @escaping completion) {
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        let task = session.dataTask(with: request){(data: Data?, response: URLResponse?, error: Error?) in
            guard let response = response as? HTTPURLResponse else{
                return
            }
            print(response.statusCode)
            OperationQueue.main.addOperation {
                completionHandler(data,response,error)
            }            
        }
        task.resume()
    }
}
