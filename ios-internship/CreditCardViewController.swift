//
//  CreditCardViewController.swift
//  ios-internship
//
//  Created by Ivan on 27.06.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class CreditCardViewController: UIViewController, UITableViewDataSource{
    
    var creditData = [Credit]()
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = creditTableView.dequeueReusableCell(withIdentifier: "CreditCell") as! CreditDataTableViewCell
        cell.nameLabel.text = creditData[indexPath.row].name
        cell.sumLabel.text = "\(creditData[indexPath.row].sum)"
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        creditData.append(Credit(contractNumber: 1, type: "CreditCard", name: "fghfgh", signDate: "dfgerg", status: "Active", debt: "2325", sum: 324))
        creditData.append(Credit(contractNumber: 1, type: "CreditCard", name: "dfgdg", signDate: "dfgerg", status: "Active", debt: "2325", sum: 355))
        creditData.append(Credit(contractNumber: 1, type: "CreditLoan", name: "hjyh", signDate: "dfgerg", status: "Active", debt: "2325", sum: 32222))
        creditData.append(Credit(contractNumber: 1, type: "CreditCard", name: "fgggh", signDate: "dfgerg", status: "Active", debt: "2325", sum: 45343))
        

        /*let nib = UINib(nibName: "CreditTableViewCell", bundle: Bundle.main)
        creditTableView.register(CreditTableViewCell.self, forCellReuseIdentifier: "new")*/
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        creditTableView.reloadData()
        /*let serverManager = ServerManager()
        serverManager.getCreditData(token: UserDefaults.standard.string(forKey: "token")!)*/
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
