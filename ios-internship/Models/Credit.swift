//
//  Credits.swift
//  ios-internship
//
//  Created by Ivan on 28.06.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import Foundation
import CoreData

struct Credit: Codable {
    var contractNumber: String = ""
    var type: String = ""
    var name: String = ""
    var signDate: String = ""
    var status: String = ""
    var debt: String?
    var sum: Double = 0
}




