//
//  enumError.swift
//  ios-internship
//
//  Created by Иван Лебедев on 06.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import Foundation

enum AuthError: Error{
    case notInternetConection
    case avtorizationError
    case invalidResponse
    case unknownError
    case invalidURL
}
