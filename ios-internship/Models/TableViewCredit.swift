//
//  TableViewCredit.swift
//  ios-internship
//
//  Created by Иван Лебедев on 19.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import Foundation

struct TableViewCredit{
    var type: String?
    var name: String?
    var debt: String?
    var sum: Double?
}
