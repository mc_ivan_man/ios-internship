//
//  URLQueries.swift
//  ios-internship
//
//  Created by Иван Лебедев on 17.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import Foundation

extension URL {
    
    func withQueryItems(_ queryItems: [URLQueryItem]) -> URL? {
        guard var urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: true) else {
            return nil
        }
        urlComponents.queryItems = queryItems
        
        return urlComponents.url
    }
}
