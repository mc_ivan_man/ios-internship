//
//  MyButton.swift
//  ios-internship
//
//  Created by Ivan on 24.06.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class MyButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        super.layer.cornerRadius = bounds.height/2
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        super.layer.cornerRadius = bounds.height/2
    }
    
    func enebled( _ enebled: Bool){
        super.isUserInteractionEnabled = enebled
        if enebled{
            super.backgroundColor = UIColor.blue
        }
        else {
            super.backgroundColor = UIColor.gray
        }
    }    
}
