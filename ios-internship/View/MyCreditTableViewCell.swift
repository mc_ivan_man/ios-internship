//
//  MyCreditTableViewCell.swift
//  ios-internship
//
//  Created by Иван Лебедев on 02.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

let formatter: NumberFormatter = {
    let _formatter = NumberFormatter()
    _formatter.locale = Locale.current
    _formatter.numberStyle = .currency
    return _formatter
}()

class MyCreditTableViewCell: UITableViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var creditImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backView.layer.cornerRadius = 10
    }
    
    func mapingCellData(credit: TableViewCredit){
        
        if credit.debt != nil{
            self.backView.backgroundColor = UIColor.red
        }
        else{
            self.backView.backgroundColor = UIColor.green
        }
        let image:UIImage? = UIImage(named: credit.type!)
        self.creditImage.image = image
        
        self.nameLabel.text = credit.name
        self.sumLabel.text = "\(formatter.string(from: credit.sum! as NSNumber)!)"
        }
}
