//
//  LoadingTableViewCell.swift
//  ios-internship
//
//  Created by Иван Лебедев on 07.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class LoadingTableViewCell: UITableViewCell {
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 10
        self.backgroundColor = .gray
    }
    
    func activationLoadIndicator(_ flag: Bool){
        
        if flag{
            self.loadingIndicator.startAnimating()
        }
        else{
            self.loadingIndicator.stopAnimating()
        }
    }
}
