//
//  CoreDataManager.swift
//  ios-internship
//
//  Created by Иван Лебедев on 09.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager {
    
    // Singleton
    static let instance = CoreDataManager()
    
    private init() {}
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: URL = {
        let appDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        return appDir
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "CreditDataModel", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let sqliteDbURL = applicationDocumentsDirectory.appendingPathComponent("CreditDataModel.sqlite")
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        try! coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: sqliteDbURL, options: nil)
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        context.persistentStoreCoordinator = coordinator
        return context
    }()
    
    // MARK: - Core Data Saving support
    func saveContext() {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    func initDBCredit(newCreditData: Credit) -> DBCredit{
        let credit = DBCredit()
        credit.contractsNumber = newCreditData.contractNumber
        credit.type = newCreditData.type
        credit.name = newCreditData.name
        credit.signDate = newCreditData.signDate
        credit.status = newCreditData.status
        credit.debt = newCreditData.debt
        credit.sum = newCreditData.sum
        return credit
    }
    
    func saveCreditDataToDB(newCreditData: [Credit]){
        if newCreditData.count == 0 { return }
        for i in 0 ..< newCreditData.count {
            let credit = initDBCredit(newCreditData: newCreditData[i])
            
            CoreDataManager.instance.managedObjectContext.insert(credit)
            try! CoreDataManager.instance.managedObjectContext.save()
        }
        
    }
    
    func myDeletDataInDB(data: [DBCredit]){
        for i in data {
            let credit = i as NSManagedObject
            CoreDataManager.instance.managedObjectContext.delete(credit)
            try! CoreDataManager.instance.managedObjectContext.save()
        }
    }
}
