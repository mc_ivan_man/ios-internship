//
//  DBCredit+CoreDataProperties.swift
//  ios-internship
//
//  Created by Иван Лебедев on 09.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//
//

import Foundation
import CoreData


extension DBCredit {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DBCredit> {
        return NSFetchRequest<DBCredit>(entityName: "DBCredit")
    }

    @NSManaged public var contractsNumber: String?
    @NSManaged public var type: String?
    @NSManaged public var name: String?
    @NSManaged public var signDate: String?
    @NSManaged public var status: String?
    @NSManaged public var debt: String?
    @NSManaged public var sum: Double

}
