//
//  DBCredit+CoreDataClass.swift
//  ios-internship
//
//  Created by Иван Лебедев on 09.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//
//

import Foundation
import CoreData

@objc(DBCredit)
public class DBCredit: NSManagedObject{
    
    convenience init() {
        // Описание сущности
        let entity = NSEntityDescription.entity(forEntityName: "DBCredit", in: CoreDataManager.instance.managedObjectContext)
        
        
        // Создание нового объекта
        self.init(entity: entity!, insertInto: CoreDataManager.instance.managedObjectContext)
    }
    
    
}
