//
//  WebServiceEndpoints.swift
//  ios-internship
//
//  Created by Иван Лебедев on 17.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import Foundation

class WebServiceEndpoints {
    
    static let shared = WebServiceEndpoints()
    
    var webServiceURL: URL {
        return URL(string: "http://jsysgnsfahfkik3xi-mock.stoplight-proxy.io/")!
    }
}
