//
//  serverManager.swift
//  ios-internship
//
//  Created by Ivan on 25.06.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import Foundation

class ServerManager{
    
    static let shared = ServerManager(host: WebServiceEndpoints.shared.webServiceURL)
    
    typealias completion = (AuthError?)-> Void
    typealias dataCompletion = (AuthError?) -> Void
    
    let url1 = "http://jsysgnsfahfkik3xi-mock.stoplight-proxy.io/api/"
    let session = MyURLSession()
    private var sessionConfiguration: URLSessionConfiguration
    private(set) var host: URL
    
    init(host: URL) {
        self.host = host
        sessionConfiguration = URLSessionConfiguration.default
    }
    
    func verificationLoginPasswod(login: String, passoword: String, completionHandler: @escaping completion){
        let queryItems = [URLQueryItem(name: "login", value: login),
                          URLQueryItem(name: "password", value: passoword)]
        
        guard let url = URL(string: "/api/auth", relativeTo: host)?.withQueryItems(queryItems) else {
            OperationQueue.main.addOperation {
                completionHandler(AuthError.invalidURL)
            }
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        session.getData(request: request){ data, response, error in
            guard let data = data else {return}
            if response?.statusCode == 403{
                completionHandler(AuthError.avtorizationError)
                return
            }
            if error == nil && response?.statusCode == 200{
                guard let dictionaryData = try? JSONDecoder().decode([String:String].self, from: data) else{
                    completionHandler(AuthError.invalidResponse)
                    return
                }
                for key in dictionaryData.keys{
                    UserDefaults.standard.set(dictionaryData[key], forKey: key)
                }
                completionHandler(nil)
            }
            else{
                let error = error as NSError?
                if error?.code == 12{
                    completionHandler(AuthError.notInternetConection)
                }
                else{
                    completionHandler(AuthError.unknownError)
                }
            }
        }
    }
    
    func getCreditData(completionHandler: @escaping dataCompletion) {
        guard let url = URL(string: "/api/contract", relativeTo: host) else {
            OperationQueue.main.addOperation {
                completionHandler(AuthError.invalidURL)
            }
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue(UserDefaults.standard.string(forKey: "token")!, forHTTPHeaderField: "Authorization")
        session.getData(request: request) { data, response, error in
            if error != nil{
                let error = error as NSError?
                if error?.code == 12{
                    completionHandler( AuthError.notInternetConection)
                }
                else{
                    completionHandler( AuthError.unknownError)
                }
            }
            if response?.statusCode == 403 || response?.statusCode == 401{
                completionHandler( AuthError.avtorizationError)
            }
            guard let data = data else {return}
            do{
                let creditData = try JSONDecoder().decode([Credit].self, from: data)
                CoreDataManager.instance.saveCreditDataToDB(newCreditData: creditData)
                completionHandler( nil)
            }catch{
                completionHandler( AuthError.invalidResponse)
            }
            
        }
        
    }
}

