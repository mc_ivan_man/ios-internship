//
//  CreditsAssembly.swift
//  ios-internship
//
//  Created by Иван Лебедев on 17.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class CreditsAssembly{
    
    func build() ->  CreditsViewController{
        let viewController = creditsViewController()
        let presenter = creditsPresenter()
        let interactor = creditsInteractor()
        let router = creditsRouter()
        
        viewController.output = presenter
        interactor.output = presenter
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        router.view = viewController
        viewController.navigationController?.isNavigationBarHidden = true
        
        return viewController
    }
    
    func creditsViewController() -> CreditsViewController{
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: "CreditsViewController") as! CreditsViewController
        
        return viewController
    }
    
    func creditsPresenter() -> CreditsPresenter{
        return CreditsPresenter()
    }
    
    func creditsInteractor() -> CreditsInteractor{
        let interactor = CreditsInteractor()
        interactor.serverManager = ServerManager.shared
        return interactor
    }
    
    func creditsRouter() -> CreditsRouter{
        let router = CreditsRouter()
        return router
    }

    
}
