//
//  CreditsRouter.swift
//  ios-internship
//
//  Created by Иван Лебедев on 18.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class CreditsRouter: ICreditsRouterInput{
    weak var view: UIViewController!
    
    func navControllerPopView() {
        view.navigationController?.popToRootViewController(animated: true)
    }
}
