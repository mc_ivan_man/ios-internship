//
//  ICreditsInteractorOutput.swift
//  ios-internship
//
//  Created by Иван Лебедев on 18.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import Foundation

protocol ICreditsInteractorOutput: class {
    
    func downloadCreditsSuccess()
    
    func downloadCreditsFailed()
}

