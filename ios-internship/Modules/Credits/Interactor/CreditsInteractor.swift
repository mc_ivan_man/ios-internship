//
//  CreditsInteractor.swift
//  ios-internship
//
//  Created by Иван Лебедев on 18.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import Foundation

class CreditsInteractor: ICreditsInteractorInput{
    
    var output: ICreditsInteractorOutput!
    
    var serverManager: ServerManager!
    
    
    func downloadCreditData() {
        serverManager.getCreditData(){ error in
            if error == nil{
                self.output.downloadCreditsSuccess()
            }
            else {
                self.output.downloadCreditsFailed()
            }
        }
    }
}
