//
//  CreditsPresenter.swift
//  ios-internship
//
//  Created by Иван Лебедев on 18.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import Foundation
import CoreData


class CreditsPresenter: ICreditsModuleInput{
    
    weak var view: CreditsViewController!
    var interactor: ICreditsInteractorInput!
    var router: ICreditsRouterInput!
    
    var creditData: [DBCredit] {
        get{
            let fetchRequest : NSFetchRequest<DBCredit> = DBCredit.fetchRequest()
            let sortDescToType = NSSortDescriptor(key: "type", ascending: true)
            let sortDescToSum = NSSortDescriptor(key: "sum", ascending: true)
            fetchRequest.sortDescriptors = [sortDescToType, sortDescToSum]
            
            let predicate = NSPredicate(format: "status != %@", "Closed")
            fetchRequest.predicate = predicate
            
            let res =  try! CoreDataManager.instance.managedObjectContext.fetch(fetchRequest)
            return res
        }
    }
   
    func configureModule() {
        
    }
    
    func exportDataToView(_ data: [DBCredit]){
        var resData = [TableViewCredit]()
        for i in 0..<creditData.count{
            resData.append(TableViewCredit(type: creditData[i].type,
            name: creditData[i].name,
            debt: creditData[i].debt,
            sum: creditData[i].sum))
        }
        view.creditData = resData
    }
}

extension CreditsPresenter: ICreditsViewOutput{
    func goToAuthViewController() {
        router.navControllerPopView()
    }
    
    func startDownloadCreditData() {
        CoreDataManager.instance.myDeletDataInDB(data: creditData)
        interactor.downloadCreditData()
    }
}

extension CreditsPresenter: ICreditsInteractorOutput{
    func downloadCreditsSuccess() {
        exportDataToView(creditData)
        view.creditsReloadData()
    }
    
    func downloadCreditsFailed() {
        view.loadAlert(title: "Ошибка загрузки данных", message: "Для повторной авторизации нажмите Назад", buttonTitle: "Назад")
    }
}
