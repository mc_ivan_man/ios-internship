//
//  CreditViewController.swift
//  ios-internship
//
//  Created by Иван Лебедев on 30.06.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit


class CreditsViewController: UIViewController, UITableViewDataSource {
    
    var output: ICreditsViewOutput!
    @IBOutlet weak var tableView: UITableView!
    var isLoading = true
    let serverManager = ServerManager.shared
    
    var creditData = [TableViewCredit]()
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        tableView.reloadData()
        
        output.startDownloadCreditData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoading{
            return creditData.count + 1
        }
        else{
            return creditData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isLoading && indexPath.row == creditData.count{
            let cell = tableView.dequeueReusableCell(withIdentifier: "loading cell") as! LoadingTableViewCell
            cell.activationLoadIndicator(true)
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "my cell id") as! MyCreditTableViewCell
            cell.mapingCellData(credit: creditData[indexPath.row])
            return cell
        }
    }
}

extension CreditsViewController: ICreditsViewInput{
    func creditsReloadData() {
        isLoading = false
        self.tableView.reloadData()
    }
    
    func loadAlert(title:String, message: String, buttonTitle: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: buttonTitle, style: .default){ alert in
            self.output.goToAuthViewController()
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
