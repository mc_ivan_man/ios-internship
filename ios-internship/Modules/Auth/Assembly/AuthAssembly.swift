//
//  AuthAssembly.swift
//  ios-internship
//
//  Created by Иван Лебедев on 16.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class AuthAssembly{
    
    func build() -> (AuthPresenter, AuthViewController){
        let viewController = authViewController()
        let presenter = authPresenter()
        let interactor = authInteractor()
        let router = authRouter()
        
        viewController.output = presenter
        interactor.output = presenter
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        router.view = viewController
        
        return (presenter, viewController)
    }
    
    func authViewController() -> AuthViewController{
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AuthViewController") as! AuthViewController
        
        return viewController
    }
    
    func authInteractor() -> AuthInteractor{
        let interactor = AuthInteractor()
        interactor.serverManager = ServerManager.shared
        return interactor
    }
    
    func authPresenter() -> AuthPresenter{
        return AuthPresenter()
    }
    
    func authRouter() -> AuthRouter{
        let router = AuthRouter()
        router.creditsAssembly = CreditsAssembly()
        return router
    }
}
