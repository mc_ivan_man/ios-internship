//
//  IAuthViewOutput.swift
//  ios-internship
//
//  Created by Иван Лебедев on 16.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

protocol IAuthViewOutput: class {
    
    func didTrigerLoginEvent()
    
    func shouldChangeCharactersIn(_ textField: UITextField, _ range: NSRange, _ string: String) -> Bool
}
