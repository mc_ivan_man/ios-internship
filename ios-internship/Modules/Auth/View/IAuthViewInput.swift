//
//  IAuthViewInput.swift
//  ios-internship
//
//  Created by Иван Лебедев on 16.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit


protocol IAuthViewInput: class {
    
    func login() -> String
    func passoword() -> String
    
    func passTextField() -> UITextField
    
    func loadAlert(title:String, message: String, buttonTitle: String)
    
    func signInButtonEnebled( _ flag: Bool)
    
    func displayDownloadIndicator(_ onOff: Bool)
}
