//
//  ViewController.swift
//  ios-internship
//
//  Created by Ivan on 24.06.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class AuthViewController: UIViewController, UITextFieldDelegate{
    
    var output: IAuthViewOutput!
    
    @IBOutlet weak var bottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passowordTextField: UITextField!    
    @IBOutlet weak var signInButton: MyButton!
    var loadView: UIView!
    
    @IBAction func signInButtonPressed(_ sender: Any) {
        
        output.didTrigerLoginEvent()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view, typically from a nib.
        loginTextField.delegate = self
        passowordTextField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWilShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWilHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        downloadIndicator()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return output.shouldChangeCharactersIn(textField, range, string)
    }
    
    @objc func keyboardWilShow(_ notification: Notification){
        myKeyboardWil(show: true, notification)
    }
    
    @objc func keyboardWilHide(_ notification: Notification){
        myKeyboardWil(show: false, notification)
    }
    
    func myKeyboardWil(show flag: Bool, _ notification: Notification) {
        let frameValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue
        let durationValue = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber
        
        guard let frame = frameValue?.cgRectValue, let duration = durationValue?.doubleValue else {
            return
        }
        
        if flag{
            keyboardWilShowHideAnimate(duration, height: -frame.height)
        }
        else{
            keyboardWilShowHideAnimate(duration, height: 0)
        }
    }
    
    func keyboardWilShowHideAnimate(_ duration: TimeInterval, height: CGFloat) {
        UIView.animate(withDuration: duration){
            self.bottomConstraints.constant = height
            self.view.layoutIfNeeded()
        }
    }
    
    func downloadIndicator(){
        let window = UIApplication.shared.keyWindow!
        loadView = UIView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
        loadView.backgroundColor = .gray
        loadView.alpha = 0.5
        
        let indicator = UIActivityIndicatorView(frame: CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
        indicator.startAnimating()
        loadView.addSubview(indicator)
    }
}

extension AuthViewController: IAuthViewInput{
    
    func login() -> String {
        return loginTextField.text ?? ""
    }
    
    func passoword() -> String {
        return passowordTextField.text ?? ""
    }
    
    func passTextField() -> UITextField {
        return passowordTextField
    }
    
    func loadAlert(title:String, message: String, buttonTitle: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: buttonTitle, style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    func signInButtonEnebled( _ flag: Bool){
        signInButton.enebled(flag)
    }
    
    func displayDownloadIndicator(_ onOff: Bool){
        let window = UIApplication.shared.keyWindow!
        if onOff{
            window.addSubview(loadView)
        }
        else{
            window.sendSubview(toBack: loadView)
        }
    }
}
