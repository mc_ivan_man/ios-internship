//
//  AuthRouter.swift
//  ios-internship
//
//  Created by Иван Лебедев on 17.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class AuthRouter: IAuthRouterInput{
    weak var view: UIViewController!
    var creditsAssembly: CreditsAssembly!
    
    func showCredits() {
        let creditsViewController = creditsAssembly.build()
        
        view.navigationController?.pushViewController(creditsViewController, animated: true)
    }
}
