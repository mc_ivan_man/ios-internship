//
//  IAuthInteractorOutput.swift
//  ios-internship
//
//  Created by Иван Лебедев on 16.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import Foundation

protocol IAuthInteractorOutput: class{
    
    func authSuccess()
    func authFailed( _ error: AuthError)
}
