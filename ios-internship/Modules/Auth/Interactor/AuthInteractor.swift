//
//  AuthInteractor.swift
//  ios-internship
//
//  Created by Иван Лебедев on 16.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import Foundation

class AuthInteractor: IAuthInteractorInput{
    
    var output: IAuthInteractorOutput!
    
    var serverManager = ServerManager.shared
    
    func auth(log: String, pass: String) {
        serverManager.verificationLoginPasswod(login: log, passoword: pass){ (error) in
            if error == nil{
                self.output.authSuccess()
            }
            else{
                self.output.authFailed(error!)
            }
        }
    }
    
}
