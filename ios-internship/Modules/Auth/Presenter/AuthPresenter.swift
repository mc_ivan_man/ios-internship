//
//  AuthPresenter.swift
//  ios-internship
//
//  Created by Иван Лебедев on 16.07.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class AuthPresenter{
    
    let kAvaiableCharacters = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890").inverted

    weak var view: AuthViewController!
    var interactor: IAuthInteractorInput!
    var router: IAuthRouterInput!
}

extension AuthPresenter: IAuthModuleInput{
    
    func configureModule() {
        
    }
}

extension AuthPresenter: IAuthViewOutput{
    
    func shouldChangeCharactersIn( _ textField: UITextField, _ range: NSRange, _ string: String) -> Bool {
        var text = textField.text ?? ""
        guard let swiftRange = Range(range,in: text) else {
            return false
        }
        text = text.replacingCharacters(in: swiftRange, with: string)
        let text2: String!
        if textField == view.passTextField(){
            text2 = view.login()
        }
        else {
            text2 = view.passoword()
        }
        view.signInButtonEnebled(!text.isEmpty && !text2.isEmpty)
        
        text = text.components(separatedBy:kAvaiableCharacters).joined()
        
        textField.text = text
        return false
    }
    
    func didTrigerLoginEvent() {
        view.displayDownloadIndicator(true)
        let login = view.login()
        let pass = view.passoword()
        interactor.auth(log: login, pass: pass)
    }
}

extension AuthPresenter: IAuthInteractorOutput{
    func authSuccess() {
        view.displayDownloadIndicator(false)
        router.showCredits()
    }
    
    func authFailed(_ error: AuthError) {
        view.displayDownloadIndicator(false)
        if error == .avtorizationError{
            view.loadAlert(title: "Неправильный логин или пароль", message: "Нажмите Назад для повторного ввода", buttonTitle: "Назад")
        }
        else if error == .notInternetConection {
            view.loadAlert(title: "Отсутствует соединение с интернетом", message: "Нажмите Назад чтобы повторить попытку", buttonTitle: "Назад")
        }
        else if error == .invalidResponse {
            view.loadAlert(title: "Ошибка HTTP запроса", message: "Нажмите Назад для повторного ввода", buttonTitle: "Назад")
        }
        else{
            view.loadAlert(title: "Ошибка", message: "Нажмите Назад для повторного ввода", buttonTitle: "Назад")
        }
    }
}
