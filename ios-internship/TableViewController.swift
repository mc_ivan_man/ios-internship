//
//  TableViewController.swift
//  ios-internship
//
//  Created by Ivan on 29.06.2018.
//  Copyright © 2018 Ivan. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    
    
    
    var creditData = [Credit]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        creditData.append(Credit(contractNumber: 1, type: "CreditCard", name: "fghfgh", signDate: "dfgerg", status: "Active", debt: "2325", sum: 324))
        creditData.append(Credit(contractNumber: 1, type: "CreditCard", name: "dfgdg", signDate: "dfgerg", status: "Active", debt: "2325", sum: 355))
        creditData.append(Credit(contractNumber: 1, type: "CreditLoan", name: "hjyh", signDate: "dfgerg", status: "Active", debt: "2325", sum: 32222))
        creditData.append(Credit(contractNumber: 1, type: "CreditCard", name: "fgggh", signDate: "dfgerg", status: "Active", debt: "2325", sum: 45343))
        
        
        /*let nib = UINib(nibName: "CreditTableViewCell", bundle: Bundle.main)
         creditTableView.register(CreditTableViewCell.self, forCellReuseIdentifier: "new")*/
        // Do any additional setup after loading the view.
        //self.navigationController?.isNavigationBarHidden = true
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return creditData.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID", for: indexPath) as! CreditTableViewCell     
        
        return cell
    }
    

    
}
